# Copyright (C) 2023 Matija Sirk

# Preprocess assets due to svg2rlg rough edges.

from io import BytesIO
from pathlib import Path
from shutil import rmtree

from PIL import Image
from reportlab.graphics import renderPM
from svglib.svglib import svg2rlg

base_dir = Path(__file__).parent

asset_dir = base_dir / "assets"
board_dir = asset_dir / "board"
piece_dir = asset_dir / "piece"

out_dir = base_dir / "built_assets"
out_board_dir = out_dir / "board"
out_piece_dir = out_dir / "piece"


PIECE_DPI = 128
BOARD_DPI = 256


def read_svg(from_file: Path, dpi: int):
    text = from_file.read_text()

    if "lineargradient" in text.lower():
        raise Exception("linearGradient not supported.")

    svgIO = BytesIO(text.encode("ascii"))

    drawing = svg2rlg(svgIO)

    if drawing is None:
        raise Exception("No svg.")

    if drawing is None:
        raise Exception("Couldn't read svg.")

    # We render twice with different backgrounds, so we get alpha.
    pngIO_green = BytesIO()
    pngIO_pink = BytesIO()

    renderPM.drawToFile(drawing, pngIO_green, fmt="PNG", dpi=dpi, bg=0x00FF00)
    pngIO_green.seek(0)
    renderPM.drawToFile(drawing, pngIO_pink, fmt="PNG", dpi=dpi, bg=0xFF00FF)
    pngIO_pink.seek(0)

    image_green = Image.open(pngIO_green).convert("RGBA")
    image_pink = Image.open(pngIO_pink).convert("RGBA")

    if image_green.width != image_green.height:
        raise Exception("Invalid aspect ratio for piece.")

    gpix = image_green.load()
    ppix = image_pink.load()

    for x in range(image_green.width):
        for y in range(image_green.height):
            if gpix[x, y] != ppix[x, y]:
                gpix[x, y] = (0, 0, 0, 0)

    return image_green


def convert_board(from_file: Path) -> Image.Image:
    if from_file.suffix == ".svg":
        image = read_svg(from_file, BOARD_DPI)
        image = image.quantize(
            colors=2
        )  # To get sharp edges. Svgs are all 2-color anyways.
    else:
        image = Image.open(from_file)

    image = image.convert("RGBA")

    return image


def preprocess_boards(in_dir: Path, out_dir: Path):
    for in_path in in_dir.glob("**/*.*"):
        try:
            board = convert_board(in_path)

            new_name = in_path.name.split(".")[0] + ".png"

            board.save(out_dir / new_name, format="png", optimize=True, quality=100)
        except:
            continue


def preprocess_piece(in_file: Path, out_paths: list[Path]):
    image = read_svg(in_file, PIECE_DPI)

    for p in out_paths:
        image.save(p, format="png", optimize=True, quality=100)


def preprocess_piece_set(in_dir: Path, out_dir: Path):
    if not out_dir.exists():
        out_dir.mkdir()

    for file in in_dir.iterdir():
        piece_name = file.name.split(".")[0]

        if piece_name.startswith("w") or piece_name.startswith("b"):
            out_paths = [out_dir / (piece_name + ".png")]
        else:
            out_paths = [
                out_dir / ("w" + piece_name + ".png"),
                out_dir / ("b" + piece_name + ".png"),
            ]

        preprocess_piece(file, out_paths)


def preprocess_pieces(in_dir: Path, out_dir: Path):
    for piece_dir in in_dir.glob("*"):
        out_piece_dir = out_dir / piece_dir.name

        try:
            preprocess_piece_set(piece_dir, out_piece_dir)
        except Exception as e:
            print(f"Err: {piece_dir.name} - {e}")
            rmtree(out_piece_dir)
            continue


if __name__ == "__main__":
    preprocess_boards(board_dir, out_board_dir)
    preprocess_pieces(piece_dir, out_piece_dir)
