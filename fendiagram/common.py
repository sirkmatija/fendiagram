# Copyright (C) 2023 Matija Sirk

import re
from io import BytesIO
from typing import Annotated, Union

import chess
from chess.svg import Arrow as SvgArrow
from fastapi import APIRouter, HTTPException
from PIL import Image
from pydantic import StringConstraints

router = APIRouter()

squareRegex = r"^[a-h][1-8]$"
filledSquareRegex = r"^[a-h][1-8]:\S+$"
moveRegex = r"^[a-h][1-8]-[a-h][1-8]$"
arrowRegex = r"^[a-h][1-8]-[a-h][1-8]:\S+$"

Square = Annotated[str, StringConstraints(pattern=squareRegex)]
Move = Annotated[str, StringConstraints(pattern=moveRegex)]
Arrow = Annotated[str, StringConstraints(pattern=arrowRegex)]
FilledSquare = Annotated[str, StringConstraints(pattern=filledSquareRegex)]


MalformedArrowException = HTTPException(400, "Arrow is malformed.")
MalformedFENException = HTTPException(400, "FEN is malformed.")


def parse_arrow(item: Union["Square", "Move", "Arrow"]) -> SvgArrow:
    if re.match(squareRegex, item) != None:
        square = chess.parse_square(item)

        return SvgArrow(square, square)

    elif re.match(moveRegex, item) != None:
        from_square, to_square = [chess.parse_square(s) for s in item.split("-")]

        return SvgArrow(from_square, to_square)

    elif re.match(arrowRegex, item) != None:
        squares, color = item.split(":")
        from_square, to_square = [chess.parse_square(s) for s in squares.split("-")]

        return SvgArrow(from_square, to_square, color=color)
    else:
        raise MalformedArrowException


# We will use versioning for display changes, so this can be unlimited.
# Consult https://simonhearne.com/2022/caching-header-best-practices/
# Be aware that CDN-s utilize query params for cache differently.
# We want them to be included.
# Cloudflare includes them, see: https://simonhearne.com/2022/caching-header-best-practices/#query-strings-and-caching
CACHE_HEADERS = {"Cache-Control": "max-age=31536000, immutable"}


def image_to_format(image: Image.Image, format: str, quality: int) -> bytes:
    formatIO = BytesIO()
    image.save(formatIO, format=format, optimize=True, quality=quality)

    # We need to return to beginning of IO before reading it.
    formatIO.seek(0)
    formatBytes = formatIO.getvalue()

    return formatBytes
