# Copyright (C) 2023 Matija Sirk


from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware

from fendiagram import fenv1, fenv2

format_to_mime = {
    "jpeg": "image/jpeg",
    "png": "image/png",
    "svg": "image/svg+xml",
    "webp": "image/webp",
}

permitted_formats_str = ", ".join([f"'.{ format}'" for format in format_to_mime.keys()])

description = f"""
FENdiagram API lets you convert FEN to {permitted_formats_str} formats.

Arrows & other graphical elements are supported, see documentation of query parameters.
"""

app = FastAPI(
    title="FENdiagram",
    description=description,
    summary=f"Convert FEN to {permitted_formats_str}.",
    version="0.0.1",
    contact={"name": "Matija Sirk", "email": "sirkmatija@gmail.com"},
    license_info={
        "name": "GNU Affero General Public License v3.0",
        "identifier": "AGPL-3.0",
    },
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["GET", "HEAD"],
    allow_headers=["*"],
)

app.add_middleware(GZipMiddleware, minimum_size=1000)

app.include_router(fenv1.router)
app.include_router(fenv2.router)


@app.get("/healthcheck")
def healtcheck():
    return {"status": "ok"}
