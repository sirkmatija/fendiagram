# Copyright (C) 2023 Matija Sirk

from dataclasses import dataclass
from io import BytesIO, StringIO
from typing import Annotated, Any, Dict, Literal, Tuple, Union

import chess
from chess import Board
from chess.svg import board
from fastapi import APIRouter, HTTPException, Path, Query, Response
from PIL import Image
from pydantic import StringConstraints
from reportlab.graphics import renderPM
from svglib.svglib import svg2rlg

from .common import *

router = APIRouter()

format_to_mime = {
    "jpeg": "image/jpeg",
    "png": "image/png",
    "svg": "image/svg+xml",
    "webp": "image/webp",
}

permitted_formats_str = ", ".join([f"'.{ format}'" for format in format_to_mime.keys()])

permitted_formats_group = "|".join([k for k in format_to_mime.keys()])

fenFormatRegex = r"^.+\.(" + permitted_formats_group + ")$"
FenFormat = Annotated[str, StringConstraints(pattern=fenFormatRegex)]

NoFormatProvidedException = HTTPException(
    400,
    f"Append desired format to path parameter. Allowed values: [{permitted_formats_str}].",
)

WrongFormatProvidedException = HTTPException(
    400,
    f"Unsopported format in path parameter. Allowed values: [{permitted_formats_str}].",
)


def parse_fenFormat(fenFormat: str) -> Tuple[str, str]:
    if "." not in fenFormat:
        raise NoFormatProvidedException

    fenRaw, formatRaw = fenFormat.split(".")

    fen = fenRaw.strip()
    format = formatRaw.strip().lower()

    if format in format_to_mime.keys():
        return (fen, format)
    else:
        raise NoFormatProvidedException


def fen_to_svg(fen: str, options: "FenDiagramQueryOptions") -> str:
    try:
        fenBoard = Board(fen)
    except:
        raise MalformedFENException

    orientation = chess.WHITE if options.orientation == "w" else chess.BLACK

    if options.lastMoveFrom != None and options.lastMoveTo != None:
        from_square = chess.parse_square(options.lastMoveFrom)
        to_square = chess.parse_square(options.lastMoveTo)

        lastmove = chess.Move(from_square, to_square)
    else:
        lastmove = None

    arrows = [parse_arrow(item) for item in options.arrows]

    fill = {
        chess.parse_square(k): v for [k, v] in [val.split(":") for val in options.fill]
    }

    squares = chess.SquareSet([chess.parse_square(sq) for sq in options.xmark])

    svgString = board(
        fenBoard,
        orientation=orientation,
        lastmove=lastmove,
        arrows=arrows,
        fill=fill,
        squares=squares,
        size=options.size,
        coordinates=options.has_coordinates,
        borders=options.has_border,
    )

    return svgString


def svg_to_png(svgString: str) -> bytes:
    svgIO = StringIO(svgString)

    drawing = svg2rlg(svgIO)

    pngIO = BytesIO()
    renderPM.drawToFile(drawing, pngIO, fmt="PNG")

    # We need to return to beginning of IO before reading it.
    pngIO.seek(0)
    pngBytes = pngIO.getvalue()

    return pngBytes


def svg_to_format(svgString: str, format: str, quality: int = 100) -> Union[str, bytes]:
    if format == "svg":
        return svgString

    pngBytes = svg_to_png(svgString)

    if format == "png":
        return pngBytes

    image = Image.open(BytesIO(pngBytes))

    formatIO = BytesIO()
    image.save(formatIO, format=format, optimize=True, quality=quality)

    # We need to return to beginning of IO before reading it.
    formatIO.seek(0)
    formatBytes = formatIO.getvalue()

    return formatBytes


@dataclass
class FenDiagramQueryOptions:
    orientation: Literal["w", "b"]
    lastMoveFrom: Union[str, None]
    lastMoveTo: Union[str, None]
    arrows: list[Union[Square, Move, Arrow]]
    fill: list[FilledSquare]
    xmark: list[Square]
    size: Union[int, None]
    has_coordinates: bool
    has_border: bool


Orientation = Literal["w", "b"]


responses: Dict[int | str, Dict[str, Any]] = {
    200: {v: {} for v in format_to_mime.values()}
}


# For decorator content consult https://stackoverflow.com/a/67497103/6212530
@router.get(
    "/fen/v1/{fenFormat:path}",
    responses=responses,  # For OpenAPI spec.
    response_class=Response,  # Prevent FastAPI from appending "application/json" to header.
)
def fendiagram(
    fenFormat: Annotated[
        FenFormat,
        Path(
            description=f"""[FEN](https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation) terminated by desired output file format. 
            Allowed formats are {permitted_formats_str}.""",
        ),
    ],
    orientation: Annotated[
        Orientation,
        Query(description="If *w* board is from white perspective, if *b* from black."),
    ] = "w",
    lastmove: Annotated[
        Union[Move, None],
        Query(
            description="Emphasize last move. Format: square-square (eg. *e2-e4*),",
        ),
    ] = None,
    arrows: Annotated[
        list[Union[Square, Move, Arrow]],
        Query(
            description="""List of arrows to draw. 
            Format: square (mark single square with circle, eg. *e4*), 
            square-square (draw arrow between squares, eg. *e2-e4*) 
            or square-square:color (draw arrow between squares in color, eg. *e2-e4:red*; for #colors use URI encoding: *e2-e4:%23ff0000*)."""
        ),
    ] = [],  # For lists query annotation is mandatory.
    fill: Annotated[
        list[FilledSquare],
        Query(
            description="""List of squares to fill with color.
            Format: square:color (eg. *e2:red*; for #colors use URI encoding: *e2:%23ff0000*)."""
        ),
    ] = [],
    xmark: Annotated[
        list[Square],
        Query(
            description="""List of squares to mark with **X**.
            Format: square (eg. *e2*)."""
        ),
    ] = [],
    size: Annotated[
        Union[int, None], Query(description="Desired size of board in pixels.")
    ] = None,
    has_coordinates: Annotated[
        bool, Query(description="If false don't show coordinates in margin.")
    ] = True,
    has_border: Annotated[
        bool, Query(description="If false don't show margin.")
    ] = True,
    quality: Annotated[
        int,
        Query(
            description="Image optimization quality. Lower leads to smaller file sizes. Not applicable for png, svg."
        ),
    ] = 100,
):
    """Version v1 uses python-chess for board rendering. Base format used is svg. Supported ouputs are webp, jpeg, png, svg."""

    fen, format = parse_fenFormat(fenFormat)

    lastMoveFrom, lastMoveTo = lastmove.split("-") if lastmove != None else (None, None)

    options = FenDiagramQueryOptions(
        orientation=orientation,
        lastMoveFrom=lastMoveFrom,
        lastMoveTo=lastMoveTo,
        arrows=arrows,
        fill=fill,
        xmark=xmark,
        size=size,
        has_coordinates=has_coordinates,
        has_border=has_border,
    )

    svgString = fen_to_svg(fen, options)

    payload = svg_to_format(svgString, format, quality)

    if format not in format_to_mime:
        raise WrongFormatProvidedException

    return Response(
        content=payload, media_type=format_to_mime[format], headers=CACHE_HEADERS
    )
