# Copyright (C) 2023 Matija Sirk

from dataclasses import dataclass
from math import floor
from pathlib import Path
from typing import Annotated, Any, Dict, Literal, Tuple, Union

from chess import WHITE, Board, square_file, square_rank
from fastapi import APIRouter, HTTPException
from fastapi import Path as PathParam
from fastapi import Query, Response
from PIL import Image, ImageDraw, ImageFont
from pydantic import StringConstraints

from .common import CACHE_HEADERS, Move, image_to_format

router = APIRouter()

format_to_mime = {
    "webp": "image/webp",
}

permitted_formats_str = ", ".join([f"'.{ format}'" for format in format_to_mime.keys()])

permitted_formats_group = "|".join([k for k in format_to_mime.keys()])

fenFormatRegex = r"^.+\.(" + permitted_formats_group + ")$"
FenFormat = Annotated[str, StringConstraints(pattern=fenFormatRegex)]

NoFormatProvidedException = HTTPException(
    400,
    f"Append desired format to path parameter. Allowed values: [{permitted_formats_str}].",
)

WrongFormatProvidedException = HTTPException(
    400,
    f"Unsopported format in path parameter. Allowed values: [{permitted_formats_str}].",
)


def parse_fenFormat(fenFormat: str) -> Tuple[str, str]:
    if "." not in fenFormat:
        raise NoFormatProvidedException

    fenRaw, formatRaw = fenFormat.split(".")

    fen = fenRaw.strip()
    format = formatRaw.strip().lower()

    if format in format_to_mime.keys():
        return (fen, format)
    else:
        raise NoFormatProvidedException


base_dir = Path(__file__).parent

asset_dir = base_dir / "built_assets"
board_dir = asset_dir / "board"
piece_dir = asset_dir / "piece"

font_dir = base_dir / "assets" / "fonts"

roboto_mono_truetype = font_dir / "RobotoMono-Regular.ttf"

boards = {p.name: p for p in board_dir.glob("**/*.*") if not p.name.startswith(".")}
pieces = {d.name: d for d in piece_dir.glob("*") if not d.name.startswith(".")}

responses: Dict[int | str, Dict[str, Any]] = {
    200: {v: {} for v in format_to_mime.values()}
}


def gather_pieces(from_dir: Path, size: int) -> dict[str, Image.Image]:
    pieces = {
        p.name.removesuffix(".png"): Image.open(p)
        .resize((size, size), Image.Resampling.LANCZOS)
        .convert("RGBA")
        for p in from_dir.iterdir()
    }

    return pieces


def get_board(from_file: Path, size: int) -> Image.Image:
    image = (
        Image.open(from_file)
        .resize((size, size), Image.Resampling.LANCZOS)
        .convert("RGBA")
    )

    return image


@dataclass
class FenDiagramOptions:
    board: Image.Image
    pieces: dict[str, Image.Image]
    fen: FenFormat
    board_size: int
    piece_size: int
    square_padding: int
    orientation: Literal["w", "b"]
    has_coords: bool
    lastmove: Union[Move, None]


def p_board_coordinates_to_pixels(
    x: int, y: int, options: FenDiagramOptions
) -> Tuple[int, int]:
    nx = x if options.orientation == "w" else 7 - x
    ny = y if options.orientation == "b" else 7 - y

    square_size = options.board_size / 8

    xtl = floor(nx * square_size + options.square_padding)
    ytl = floor(ny * square_size + options.square_padding)

    return (xtl, ytl)


def draw_pieces(image: Image.Image, options: FenDiagramOptions):
    board = Board(options.fen)

    piece_map = board.piece_map()

    drawables = [
        [square_file(k), square_rank(k), "w" if v.color == WHITE else "b", v.symbol()]
        for [k, v] in piece_map.items()
    ]

    with_img = [
        [
            p_board_coordinates_to_pixels(x, y, options),
            f"{c}{p.upper()}",
        ]
        for [x, y, c, p] in drawables
    ]

    for [coords, imgkey] in with_img:
        img = options.pieces[imgkey]
        image.paste(img, coords, img)

    return image


white_row_marks = ["a", "b", "c", "d", "e", "f", "g", "h"]
white_col_marks = [1, 2, 3, 4, 5, 6, 7, 8]
black_row_marks = [*white_row_marks]
black_col_marks = [*white_col_marks]

black_row_marks.reverse()
white_col_marks.reverse()


def get_p_bl_corner(x: int, y: int, font_size: int, options: FenDiagramOptions):
    square_size = options.board_size / 8

    xtl = floor((x) * square_size + font_size / 4)
    ytl = floor((y + 1) * square_size - 1.4 * font_size)

    return (xtl, ytl)


def get_p_tr_corner(x: int, y: int, font_size: int, options: FenDiagramOptions):
    square_size = options.board_size / 8

    xtl = floor((x + 1) * square_size - font_size * 0.8)
    ytl = floor((y) * square_size + font_size / 6)

    return (xtl, ytl)


def get_tl_corner(x: int, y: int, options: FenDiagramOptions):
    square_size = options.board_size / 8

    xtl = floor((x) * square_size)
    ytl = floor((y) * square_size)

    return (xtl, ytl)


def draw_text(
    image: Image.Image, coords: Tuple[int, int], font_size: int, content: str, color
):
    base = Image.new("RGBA", image.size, (255, 255, 255, 0))

    fnt = ImageFont.truetype(
        roboto_mono_truetype.absolute().resolve().__str__(), font_size
    )

    d = ImageDraw.Draw(base)

    d.text(coords, content, font=fnt, fill=color)

    return Image.alpha_composite(image, base)


def square_to_xy(square: str) -> Tuple[int, int]:
    xs = square[0]
    ys = square[1]

    x = white_row_marks.index(xs)
    y = white_col_marks.index(int(ys))

    return (x, y)


def is_square_light(x: int, y: int) -> bool:
    return ((x + y) % 2) == 0


def fill_square(
    image: Image.Image,
    coords: Tuple[int, int],
    options: FenDiagramOptions,
    color: Tuple[int, int, int, int],
):
    base = Image.new("RGBA", image.size, (255, 255, 255, 0))

    d = ImageDraw.Draw(base)

    tl = get_tl_corner(coords[0], coords[1], options)
    br = (tl[0] + options.board_size / 8, tl[1] + options.board_size / 8)

    d.rectangle((tl, br), fill=color)

    return Image.alpha_composite(image, base)


color_last_move_light = (170, 162, 59, 80)
color_last_move_dark = (205, 209, 106, 80)


def draw_lastmove(image: Image.Image, options: FenDiagramOptions):
    if not options.lastmove:
        return image

    from_square, to_square = options.lastmove.split("-")

    from_coords, to_coords = square_to_xy(from_square), square_to_xy(to_square)

    from_fill = (
        color_last_move_light if is_square_light(*from_coords) else color_last_move_dark
    )
    to_fill = (
        color_last_move_light if is_square_light(*to_coords) else color_last_move_dark
    )

    image = fill_square(image, from_coords, options, from_fill)
    image = fill_square(image, to_coords, options, to_fill)

    return image


def draw_coords(image: Image.Image, options: FenDiagramOptions):
    # Coordinates we draw in bottom row in bottom left corner (letters)
    # And in right column in upper right corner (numbers)

    row_marks = white_row_marks if options.orientation == "w" else black_row_marks
    col_marks = white_col_marks if options.orientation == "w" else black_col_marks

    font_size = floor(15 * (options.board_size / 600))

    for i in range(8):
        row_coords = get_p_bl_corner(i, 7, font_size, options)
        col_coords = get_p_tr_corner(7, i, font_size, options)

        r = row_marks[i]
        c = f"{col_marks[i]}"

        image = draw_text(image, row_coords, font_size, r, (255, 255, 255, 160))
        image = draw_text(image, col_coords, font_size, c, (255, 255, 255, 160))

    return image


def draw_fen(options: FenDiagramOptions) -> Image.Image:
    image = options.board.convert("RGBA")

    if options.has_coords:
        image = draw_coords(image, options)

    if options.lastmove:
        image = draw_lastmove(image, options)

    image = draw_pieces(image, options)

    return image


available_boards = [b.removesuffix(".png") for b in boards.keys()]
available_piecesets = [p for p in pieces.keys()]


# For decorator content consult https://stackoverflow.com/a/67497103/6212530
@router.get(
    "/fen/v2/{fenFormat:path}",
    responses=responses,  # For OpenAPI spec.
    response_class=Response,  # Prevent FastAPI from appending "application/json" to header.
)
def fendiagram(
    fenFormat: Annotated[
        FenFormat,
        PathParam(
            description=f"""[FEN](https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation) terminated by desired output file format. 
            Allowed formats are {permitted_formats_str}. Example: rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1.webp""",
        ),
    ],
    board: Annotated[
        str,
        Query(
            description=f"""Board background to use.
                                Available: {", ".join(available_boards)}."""
        ),
    ] = "blue2",
    piece: Annotated[
        str,
        Query(
            description=f"""Piece set to use.
                                Available: {", ".join(available_piecesets)}."""
        ),
    ] = "fresca",
    size: Annotated[int, Query(description="Size of diagram in pixels.")] = 400,
    quality: Annotated[
        int, Query(description="Quality of diagram from 0 to 100.")
    ] = 100,
    orientation: Annotated[
        Literal["w", "b"],
        Query(description="If w board is from white perspective, if b from black."),
    ] = "w",
    has_coords: Annotated[
        bool, Query(description="Set to 0 to disable coordinate marks.")
    ] = True,
    lastmove: Annotated[
        Union[Move, None],
        Query(
            description="Emphasize last move. Format: square-square (eg. *e2-e4*),",
        ),
    ] = None,
):
    """Version v2 uses custom bitmap rendering for board rendering. Supported ouput is webp."""
    fen, format = parse_fenFormat(fenFormat)

    if board + ".png" not in boards.keys():
        raise HTTPException(400, "Board not found.")

    if piece not in pieces.keys():
        raise HTTPException(400, "Pieces not found.")

    board_img = get_board(boards[board + ".png"], size)

    square_padding = floor((size / 8) * 0.05)
    piece_size = floor((size / 8) - 2 * square_padding)

    piece_imgs = gather_pieces(pieces[piece], piece_size)

    options = FenDiagramOptions(
        board=board_img,
        pieces=piece_imgs,
        fen=fen,
        board_size=size,
        piece_size=piece_size,
        square_padding=square_padding,
        orientation=orientation,
        has_coords=has_coords,
        lastmove=lastmove,
    )

    fen_img = draw_fen(options)

    payload = image_to_format(fen_img, format, quality)

    return Response(
        content=payload, media_type=format_to_mime[format], headers=CACHE_HEADERS
    )
