# See https://hub.docker.com/_/python/

FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

# RUN python ./fendiagram/preprocess.py

CMD ["uvicorn", "fendiagram.main:app", "--host", "0.0.0.0", "--port", "8080"]